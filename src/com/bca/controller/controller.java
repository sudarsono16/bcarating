package com.bca.controller;

import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.google.gson.Gson;

import adapter.LogAdapter;
import adapter.RatingAdapter;

@RestController
@RequestMapping("/sequence")
public class controller {
    final static Logger logger = LogManager.getLogger(controller.class);

    @RequestMapping(value = "/ping", method = RequestMethod.POST, consumes = "application/json", headers = "Accept=application/json")
    public @ResponseBody String GetPing() {
	String ConnectionStatus = "true";
	return ConnectionStatus;
    }

    @RequestMapping(value = "", method = RequestMethod.POST, consumes = "application/json", headers = "Accept=application/json")
    public @ResponseBody model.mdlAPIResult APIInsertRating(@RequestBody model.mdlRating param, HttpServletResponse response) {
	long startTime = System.currentTimeMillis();
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	model.mdlAPIResult mdlRatingResult = new model.mdlAPIResult();
	model.mdlErrorSchema mdlErrorSchema = new model.mdlErrorSchema();
	model.mdlMessage mdlMessage = new model.mdlMessage();
	model.mdlResult mdlResult = new model.mdlResult();
	Gson gson = new Gson();

	String apiName = "/selvi-rating";
	String apiMethod = "POST";

	model.mdlLog mdlLog = new model.mdlLog();
	mdlLog.WSID = param.WSID;
	mdlLog.SerialNumber = param.SerialNumber;
	mdlLog.ApiFunction = "Rating";
	mdlLog.SystemFunction = "APIInsertRating";
	mdlLog.LogSource = "Webservice";
	mdlLog.LogStatus = "Failed";
	mdlResult.Result = "Failed";

	String sequenceID = "";
	String jsonIn = gson.toJson(param);

	if ((param.SerialNumber == null || param.SerialNumber.equals("")) || (param.WSID == null || param.WSID.equals(""))) {
	    mdlErrorSchema.ErrorCode = "01";
	    mdlMessage.Indonesian = "Serial Number / WSID kosong";
	    mdlMessage.English = mdlLog.ErrorMessage = "Serial Number / WSID empty";
	    mdlErrorSchema.ErrorMessage = mdlMessage;
	    mdlRatingResult.ErrorSchema = mdlErrorSchema;
	    mdlRatingResult.OutputSchema = mdlResult;
	    LogAdapter.InsertLog(mdlLog);
	    String jsonOut = gson.toJson(mdlRatingResult);
	    logger.error(LogAdapter.logControllerToLog4j(false, startTime, 400, apiName, apiMethod, "function: " + functionName, jsonIn, jsonOut));
	    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    return mdlRatingResult;
	}

	try {
	    RatingAdapter.CheckLastRating(param.WSID);

	    if (param.SequenceID == null || param.SequenceID.equals("")) {
		param.SequenceID = RatingAdapter.CreateSequenceID(param.WSID, "");
		sequenceID = RatingAdapter.InsertRating(param);
	    } else {
		Boolean isExists = RatingAdapter.CheckSequenceID(param.SequenceID);

		if (isExists) {
		    param.SequenceID = RatingAdapter.CreateSequenceID(param.WSID, param.SequenceID);
		    sequenceID = RatingAdapter.InsertRating(param);
		} else {
		    sequenceID = RatingAdapter.InsertRating(param);
		}
	    }

	    if (!sequenceID.equalsIgnoreCase("")) {
		mdlErrorSchema.ErrorCode = "00";
		mdlMessage.Indonesian = "Rating berhasil disimpan";
		mdlMessage.English = mdlLog.ErrorMessage = "Rating successfully saved";
		mdlErrorSchema.ErrorMessage = mdlMessage;
		mdlRatingResult.ErrorSchema = mdlErrorSchema;
		mdlResult.Result = sequenceID;
		mdlRatingResult.OutputSchema = mdlResult;

		mdlLog.LogStatus = "Success";
		String jsonOut = gson.toJson(mdlRatingResult);
		logger.info(LogAdapter.logControllerToLog4j(true, startTime, 200, apiName, apiMethod, "function: " + functionName, jsonIn, jsonOut));
	    } else {
		mdlErrorSchema.ErrorCode = "02";
		mdlMessage.Indonesian = "Rating gagal disimpan";
		mdlMessage.English = mdlLog.ErrorMessage = "Save rating failed";
		mdlErrorSchema.ErrorMessage = mdlMessage;
		mdlRatingResult.ErrorSchema = mdlErrorSchema;
		mdlRatingResult.OutputSchema = mdlResult;
		String jsonOut = gson.toJson(mdlRatingResult);
		logger.error(LogAdapter.logControllerToLog4j(false, startTime, 400, apiName, apiMethod, "function: " + functionName, jsonIn, jsonOut));
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    }
	} catch (Exception ex) {
	    mdlErrorSchema.ErrorCode = "03";
	    mdlMessage.Indonesian = "Gagal memanggil service";
	    mdlMessage.English = mdlLog.ErrorMessage = "Service call failed";
	    mdlErrorSchema.ErrorMessage = mdlMessage;
	    mdlRatingResult.ErrorSchema = mdlErrorSchema;
	    mdlRatingResult.OutputSchema = mdlResult;
	    String jsonOut = gson.toJson(mdlRatingResult);
	    logger.error(LogAdapter.logControllerToLog4jException(startTime, 500, apiName, apiMethod, "", jsonIn, jsonOut, ex.toString()), ex);
	    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	}
	LogAdapter.InsertLog(mdlLog);
	return mdlRatingResult;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, consumes = "application/json", headers = "Accept=application/json")
    public @ResponseBody model.mdlAPIResult APIUpdateRating(@PathVariable("id") String sequenceID, @RequestBody model.mdlRating param,
	    HttpServletResponse response) {
	long startTime = System.currentTimeMillis();
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	model.mdlAPIResult mdlRatingResult = new model.mdlAPIResult();
	model.mdlErrorSchema mdlErrorSchema = new model.mdlErrorSchema();
	model.mdlMessage mdlMessage = new model.mdlMessage();
	model.mdlResult mdlResult = new model.mdlResult();
	Gson gson = new Gson();
	
	String apiName = "/selvi-rating/" + sequenceID;
	String apiMethod = "PUT";

	model.mdlLog mdlLog = new model.mdlLog();
	param.SequenceID = sequenceID;
	mdlLog.WSID = param.WSID;
	mdlLog.SerialNumber = param.SerialNumber;
	mdlLog.ApiFunction = "Rating";
	mdlLog.SystemFunction = "APIUpdateRating";
	mdlLog.LogSource = "Webservice";
	mdlLog.LogStatus = "Failed";	
	mdlResult.Result = "Failed";

	Boolean success = false;
	String jsonIn = gson.toJson(param);

	if (param.SequenceID == null || param.SequenceID.equalsIgnoreCase("")) {
	    mdlErrorSchema.ErrorCode = "01";
	    mdlMessage.Indonesian = "Sequence ID kosong";
	    mdlMessage.English = mdlLog.ErrorMessage = "Sequence ID empty";
	    mdlErrorSchema.ErrorMessage = mdlMessage;
	    mdlRatingResult.ErrorSchema = mdlErrorSchema;
	    mdlRatingResult.OutputSchema = mdlResult;
	    LogAdapter.InsertLog(mdlLog);
	    String jsonOut = gson.toJson(mdlRatingResult);
	    logger.error(LogAdapter.logControllerToLog4j(false, startTime, 400, apiName, apiMethod, "function: " + functionName, jsonIn, jsonOut));
	    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    return mdlRatingResult;
	}

	try {
	    success = RatingAdapter.UpdateRating(param);

	    if (success) {
		mdlErrorSchema.ErrorCode = "00";
		mdlMessage.Indonesian = "Rating berhasil diupdate";
		mdlMessage.English = mdlLog.ErrorMessage = "Rating successfully updated";
		mdlErrorSchema.ErrorMessage = mdlMessage;
		mdlRatingResult.ErrorSchema = mdlErrorSchema;
		mdlResult.Result = "Success";
		mdlRatingResult.OutputSchema = mdlResult;

		mdlLog.LogStatus = "Success";
		String jsonOut = gson.toJson(mdlRatingResult);
		logger.info(LogAdapter.logControllerToLog4j(true, startTime, 200, apiName, apiMethod, "function: " + functionName, jsonIn, jsonOut));
	    } else {
		mdlErrorSchema.ErrorCode = "02";
		mdlMessage.Indonesian = "Rating gagal diupdate";
		mdlMessage.English = mdlLog.ErrorMessage = "Update rating failed";
		mdlErrorSchema.ErrorMessage = mdlMessage;
		mdlRatingResult.ErrorSchema = mdlErrorSchema;
		mdlRatingResult.OutputSchema = mdlResult;
		String jsonOut = gson.toJson(mdlRatingResult);
		logger.error(LogAdapter.logControllerToLog4j(false, startTime, 400, apiName, apiMethod, "function: " + functionName, jsonIn, jsonOut));
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    }
	} catch (Exception ex) {
	    mdlErrorSchema.ErrorCode = "03";
	    mdlMessage.Indonesian = "Gagal memanggil service";
	    mdlMessage.English = mdlLog.ErrorMessage = "Service call failed";
	    mdlErrorSchema.ErrorMessage = mdlMessage;
	    mdlRatingResult.ErrorSchema = mdlErrorSchema;
	    mdlRatingResult.OutputSchema = mdlResult;
	    String jsonOut = gson.toJson(mdlRatingResult);
	    logger.error(LogAdapter.logControllerToLog4jException(startTime, 500, apiName, apiMethod, "", jsonIn, jsonOut, ex.toString()), ex);
	    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	}
	LogAdapter.InsertLog(mdlLog);
	return mdlRatingResult;
    }
}
