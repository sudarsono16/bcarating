package adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDateTime;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import com.google.gson.Gson;

public class RatingAdapter {
    final static Logger logger = LogManager.getLogger(RatingAdapter.class);
    static String apiName = "/selvi-rating";
    static String apiMethod = "POST";

    public static Boolean CheckRating(String SequenceID) {
	long startTime = System.currentTimeMillis();
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	Connection connection = null;
	PreparedStatement pstm = null;
	ResultSet jrs = null;
	Boolean isNew = true;
	try {
	    // define connection
	    connection = database.RowSetAdapter.getConnectionWL();
	    String sql = "SELECT 1 FROM TRANSACTIONSEQUENCE WHERE SequenceID = ?";
	    pstm = connection.prepareStatement(sql);

	    // insert sql parameter
	    pstm.setString(1, SequenceID);

	    // execute query
	    jrs = pstm.executeQuery();

	    while (jrs.next()) {
		isNew = false;
	    }
	} catch (Exception ex) {
	    logger.error(LogAdapter.logToLog4jException(startTime, 500, "/selvi-rating", "POST", "function: " + functionName + ", SequenceID : "
		    + SequenceID, "", "", ex.toString()), ex);
	} finally {
	    try {
		// close the opened connection
		if (pstm != null)
		    pstm.close();
		if (connection != null)
		    connection.close();
		if (jrs != null)
		    jrs.close();
	    } catch (Exception e) {
		logger.error(LogAdapter.logToLog4jException(startTime, 500, "/selvi-rating", "POST", "function: " + functionName, "", "", e.toString()), e);
	    }
	}
	return isNew;
    }

    public static String InsertRating(model.mdlRating mdlRating) {
	long startTime = System.currentTimeMillis();
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	String sequenceID = "";
	Connection connection = null;
	PreparedStatement pstm = null;
	ResultSet jrs = null;
	Gson gson = new Gson();

	try {
	    String dateNow = LocalDateTime.now().toString().replace("T", " ");
	    String startTimeSequence = mdlRating.StartTime == null || mdlRating.StartTime.equals("") ? dateNow : mdlRating.StartTime;
	    // define connection
	    connection = database.RowSetAdapter.getConnectionWL();
	    String sql = "INSERT INTO TRANSACTIONSEQUENCE (SequenceID, WSID, SerialNumber, StartTime)"
		    + "VALUES (?,?,?,TO_TIMESTAMP(?,'YYYY-MM-DD HH24:MI:SS.FF') )";
	    pstm = connection.prepareStatement(sql);

	    // insert sql parameter
	    pstm.setString(1, mdlRating.SequenceID);
	    pstm.setString(2, mdlRating.WSID);
	    pstm.setString(3, mdlRating.SerialNumber);
	    pstm.setString(4, startTimeSequence);

	    // execute query
	    jrs = pstm.executeQuery();
	    sequenceID = mdlRating.SequenceID;
	} catch (Exception ex) {
	    logger.error(LogAdapter.logToLog4jException(startTime, 500, "/selvi-rating", "POST", "function: "
		    + functionName, gson.toJson(mdlRating), "", ex.toString()), ex);
	} finally {
	    try {
		// close the opened connection
		if (pstm != null)
		    pstm.close();
		if (connection != null)
		    connection.close();
		if (jrs != null)
		    jrs.close();
	    } catch (Exception e) {
		logger.error(LogAdapter.logToLog4jException(startTime, 500, "/selvi-rating", "POST", "function: " + functionName, "", "", e.toString()), e);
	    }
	}
	return sequenceID;
    }

    public static Boolean UpdateRating(model.mdlRating mdlRating) {
	long startTime = System.currentTimeMillis();
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	Boolean success = false;
	Connection connection = null;
	PreparedStatement pstm = null;
	ResultSet jrs = null;
	Gson gson = new Gson();
	try {
	    String dateNow = LocalDateTime.now().toString().replace("T", " ");
	    String endTime = mdlRating.EndTime == null || mdlRating.EndTime.equals("") ? dateNow : mdlRating.EndTime;
	    Integer rating = mdlRating.Rating == null ? 0 : mdlRating.Rating;
	    String comment1 = mdlRating.Comment1 == null ? "" : mdlRating.Comment1;
	    String comment2 = mdlRating.Comment2 == null ? "" : mdlRating.Comment2;
	    String status = mdlRating.Status == null || mdlRating.Status.equals("") ? "TIMEOUT" : mdlRating.Status;
	    String productCode = mdlRating.RecommendationProductCode == null ? "" : mdlRating.RecommendationProductCode;
	    String productName = mdlRating.RecommendationProductName == null ? "" : mdlRating.RecommendationProductName;
	    String campaignCode = mdlRating.RecommendationCampaignCode == null ? "" : mdlRating.RecommendationCampaignCode;
	    String campaignName = mdlRating.RecommendationCampaignName == null ? "" : mdlRating.RecommendationCampaignName;
	    String productResonse = mdlRating.RecommendationProductResponse == null ? "Noresponse" : mdlRating.RecommendationProductResponse;
	    String referenceNo = mdlRating.ReferenceNo == null ? "" : mdlRating.ReferenceNo;

	    // define connection
	    connection = database.RowSetAdapter.getConnectionWL();
	    String sql = "UPDATE TRANSACTIONSEQUENCE SET EndTime = TO_TIMESTAMP(?,'YYYY-MM-DD HH24:MI:SS.FF'), Rating = ?, "
		    + "Comment1 = ?, Comment2 = ?, "
		    + "Status = ?, UpdatedDate = TO_TIMESTAMP(?,'YYYY-MM-DD HH24:MI:SS.FF'), "
		    + "RecommendationProductCode = ?, RecommendationProductName = ?, RecommendationProductResponse = ?, "
		    + "ReferenceNo = ?, RecommendationCampaignCode = ?, RecommendationCampaignName = ? WHERE SequenceID = ?";
	    pstm = connection.prepareStatement(sql);

	    // insert sql parameter
	    pstm.setString(1, endTime);
	    pstm.setInt(2, rating);
	    pstm.setString(3, comment1);
	    pstm.setString(4, comment2);
	    pstm.setString(5, status);
	    pstm.setString(6, dateNow);
	    pstm.setString(7, productCode);
	    pstm.setString(8, productName);
	    pstm.setString(9, productResonse);
	    pstm.setString(10, referenceNo);
	    pstm.setString(11, campaignCode);
	    pstm.setString(12, campaignName);
	    pstm.setString(13, mdlRating.SequenceID);

	    // execute query
	    jrs = pstm.executeQuery();
	    success = true;
	} catch (Exception ex) {
	    logger.error(LogAdapter.logToLog4jException(startTime, 500, "/selvi-rating", "POST", "function: "
		    + functionName, gson.toJson(mdlRating), "", ex.toString()), ex);
	} finally {
	    try {
		// close the opened connection
		if (pstm != null)
		    pstm.close();
		if (connection != null)
		    connection.close();
		if (jrs != null)
		    jrs.close();
	    } catch (Exception e) {
		logger.error(LogAdapter.logToLog4jException(startTime, 500, "/selvi-rating", "POST", "function: " + functionName, "", "", e.toString()), e);
	    }
	}
	return success;
    }

    public static String CreateSequenceID(String WSID, String sequenceID) {
	long startTime = System.currentTimeMillis();
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	String SequenceID = "";
	try {
	    String sequenceDate = "";
	    if (sequenceID.equals("")) {
		String dateNow = LocalDateTime.now().toString().substring(0, 10).toString();
		String datetime = ConvertDateTimeHelper.formatDate(dateNow, "yyyy-MM-dd", "yyyyMMdd");
		sequenceDate = datetime;
	    } else {
		sequenceDate = sequenceID.substring(13, 21);
	    }

	    String lastSequenceID = GetLastSequenceID(WSID, sequenceDate);
	    String stringIncrement = "0001";
	    String transactionWSID = WSID == null || WSID.equals("") ? "00000000" : WSID;

	    if ((lastSequenceID != null) && (!lastSequenceID.equals(""))) {
		String[] partsSequenceID = lastSequenceID.split("-");
		// get datetime and last running number log for the related WSID
		String partNumber = partsSequenceID[3];
		Integer inc = Integer.parseInt(partNumber) + 1;
		stringIncrement = String.format("%04d", inc);
	    }
	    StringBuilder sb = new StringBuilder();
	    sb.append("SEQ-").append(transactionWSID).append("-").append(sequenceDate).append("-").append(stringIncrement);
	    SequenceID = sb.toString();
	} catch (Exception ex) {
	    logger.error(LogAdapter.logToLog4jException(startTime, 500, "/selvi-rating", "POST", "function: " + functionName, "", "", ex.toString()), ex);
	}
	return SequenceID;
    }

    public static String GetLastSequenceID(String WSID, String date) {
	Connection connection = null;
	PreparedStatement pstm = null;
	ResultSet jrs = null;
	String SequenceID = "";
	try {
	    connection = database.RowSetAdapter.getConnectionWL();
	    pstm = connection.prepareStatement("SELECT NVL(MAX(SequenceID),'') as SequenceID FROM TRANSACTIONSEQUENCE WHERE WSID = ? "
		    + "AND SUBSTR(SequenceID,14,8) = ? ");
	    pstm.setString(1, WSID);
	    pstm.setString(2, date);

	    jrs = pstm.executeQuery();

	    while (jrs.next()) {
		SequenceID = jrs.getString("SequenceID");
	    }
	} catch (Exception ex) {
	    logger.error("FAILED. API : BCARating, method : POST, function: GetLastSequenceID, WSID : " + WSID
		    + ", sequenceDate : " + date + ",Exception : " + ex.toString(), ex);
	} finally {
	    // close the opened connection
	    try {
		// close the opened connection
		if (pstm != null)
		    pstm.close();
		if (connection != null)
		    connection.close();
		if (jrs != null)
		    jrs.close();
	    } catch (Exception ex) {

	    }
	}
	return SequenceID;
    }

    public static void CheckLastRating(String WSID) {
	long startTime = System.currentTimeMillis();
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	Connection connection = null;
	PreparedStatement pstm = null;
	ResultSet jrs = null;

	String dateNow = LocalDateTime.now().toString().replace("T", " ");
	try {
	    connection = database.RowSetAdapter.getConnectionWL();
	    pstm = connection.prepareStatement("UPDATE TRANSACTIONSEQUENCE SET STATUS = 'FORCED', "
		    + "UpdatedDate = TO_TIMESTAMP(?,'YYYY-MM-DD HH24:MI:SS.FF') "
		    + "WHERE WSID = ? AND StartTime IS NOT NULL AND EndTime IS NULL AND RATING IS NULL AND STATUS IS NULL");
	    pstm.setString(1, dateNow);
	    pstm.setString(2, WSID);

	    jrs = pstm.executeQuery();
	} catch (Exception ex) {
	    logger.error(LogAdapter.logToLog4jException(startTime, 500, "/selvi-rating", "POST", "function: " + functionName, "", "", ex.toString()), ex);
	} finally {
	    // close the opened connection
	    try {
		// close the opened connection
		if (pstm != null)
		    pstm.close();
		if (connection != null)
		    connection.close();
		if (jrs != null)
		    jrs.close();
	    } catch (Exception ex) {
		logger.error(LogAdapter.logToLog4jException(startTime, 500, "/selvi-rating", "POST", "function: " + functionName, "", "", ex.toString()), ex);
	    }
	}
    }

    public static Boolean CheckSequenceID(String sequenceID) {
	long startTime = System.currentTimeMillis();
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	Connection connection = null;
	PreparedStatement pstm = null;
	ResultSet jrs = null;
	Boolean isExists = false;

	try {
	    connection = database.RowSetAdapter.getConnectionWL();
	    pstm = connection.prepareStatement("SELECT SequenceID FROM TRANSACTIONSEQUENCE WHERE SequenceID = ? ");
	    pstm.setString(1, sequenceID);

	    jrs = pstm.executeQuery();
	    while (jrs.next()) {
		isExists = true;
	    }
	} catch (Exception ex) {
	    logger.error(LogAdapter.logToLog4jException(startTime, 500, "/selvi-rating", "POST", "function: " + functionName, "", "", ex.toString()), ex);
	} finally {
	    // close the opened connection
	    try {
		// close the opened connection
		if (pstm != null)
		    pstm.close();
		if (connection != null)
		    connection.close();
		if (jrs != null)
		    jrs.close();
	    } catch (Exception ex) {
		logger.error(LogAdapter.logToLog4jException(startTime, 500, "/selvi-rating", "POST", "function: " + functionName, "", "", ex.toString()), ex);
	    }
	}
	return isExists;
    }
}
